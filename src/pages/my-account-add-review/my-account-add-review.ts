import { Component ,ViewChild ,ElementRef, OnInit, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { AppConst } from '../../AppConst';
import { AppState } from '../../AppStates';
/**
 * Generated class for the MyAccountAddReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-account-add-review',
  templateUrl: 'my-account-add-review.html',
})
export class MyAccountAddReviewPage {
  selectedCompany: any;
  rating:any
  comments:any
  companypicture: string;
  constructor(public navCtrl: NavController, private toastCtrl :ToastController,private apiProvider:ApiProvider,private modalCtrl :ModalController,public navParams: NavParams) {
    console.log(navParams.data)
    this.selectedCompany=navParams.data['3'];

    if(this.selectedCompany.hasOwnProperty('companylogo')){
      if(this.selectedCompany.companylogo !=''){
       this.companypicture= this.selectedCompany.companylogo;
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAccountAddReviewPage');
    this.companypicture= AppConst.WEMA_DEV_ROOT+"images/no-service-icon-black.png"
  }

  SelectCompany()
{
 
  console.log(this.rating)
  var bookServicePopup = this.modalCtrl.create('SelectComapanyForareviewPage',this.navParams.data, { enableBackdropDismiss: false });
  bookServicePopup.onDidDismiss(async (data) => {
    console.log(data)
    this.selectedCompany=data;
    if(this.selectedCompany.hasOwnProperty('companylogo')){
      if(this.selectedCompany.companylogo !=''){
       this.companypicture= this.selectedCompany.companylogo;
      }
    }



  });
  bookServicePopup.present();
  // this.navCtrl.push(SelectComapanyForareviewPage,this.navParams.data)
}

  submit() 
{

  if (this.comments == null && this.comments == '') {
    this.toastCtrl.create({
      message: 'Please give comment about service',
      duration: 2000
    }).present();
    return;
  }
  else
  {

    this.AddReview()
  }
 

}
async AddReview()
{
let request=
{
  ApptSource: "wemalife",
  action: "rating",
  appointmentid: this.selectedCompany.appointmentid,
  comments: this.comments,
  companyid: this.selectedCompany.companyid,
  rating: this.rating
};

let response = await this.apiProvider.Post( AppConst.ADD_OR_UPDATE_COMMENTS,request).toPromise()
if (response != null && response['status']) {
      this.toastCtrl.create({
        message: 'Thanks for rating',
        duration: 2000
      }).present();
      this.navCtrl.pop();
    }
  }
}
