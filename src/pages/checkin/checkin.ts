import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , AlertController ,ModalController, ToastController, Events, Modal } from 'ionic-angular';
import { AppConst } from '../../AppConst';
import { ApiProvider } from '../../providers/api/api';
import { DatePipe } from "@angular/common";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AppState } from '../../AppStates';
import { ProviderCheckinSuccessPage } from '../provider-checkin-success/provider-checkin-success';
import { ModelPage } from '../model/model';
import { CheckinReasonPopupPage } from '../checkin-reason-popup/checkin-reason-popup';
/**
 * Generated class for the CheckinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkin',
  templateUrl: 'checkin.html',
})
export class CheckinPage {
  appointment: any;
  selector:boolean
  digit1:any
  digit2:any
  digit3:any
  digit4:any
  otp4:any
  ckeckindata: boolean;
  finalotp: string;
  mainOtp: any;
  sendSMs: boolean;
  constructor(public navCtrl: NavController,private datePipe: DatePipe, private barcodeScanner: BarcodeScanner, private apiProvider: ApiProvider, public alertCtrl: AlertController, public navParams: NavParams, private modalCtrl: ModalController, private toastCtrl: ToastController) {
  console.log(navParams.data)
  this.appointment=navParams.data
  console.log('------------ Appointment Data ----------------',this.appointment)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckinPage');
  }


  
  /**
   * CheckIn
   * @param appointment 
   */
  checkIn(appointment: any) {
    if (appointment != null) {
      if (appointment.status == "5" || appointment.status == "3" || appointment.status == "8") return;

      this.barcodeScanner.scan().then((data) => {
      
        if (data != null) {
          console.log('-----------bar code scanner ------------- ',data)
          let appointmentIds = appointment.id.split('_');
          let scanResult = data.text.split('||');
          console.log(scanResult,appointmentIds)
          if (scanResult[1] == appointmentIds[appointmentIds.length - 1]) {
            //if (scanResult[scanResult.length - 1] == 'true') {              
            if (scanResult['5'] == 'true') {
              
              this.getReasonForLatePopup(scanResult, appointment, 'early');
            }
            //else if (scanResult[scanResult.length - 2] == 'true') {
            else if (scanResult['4'] == 'true') {
              this.getReasonForLatePopup(scanResult, appointment, 'late')
            }
            else {
              this.executeCheckIn(scanResult, appointment, null, null)
            }
          }
          else {
            console.log('in mismatch')
         //  this.getReasonForLatePopup(scanResult, appointment, 'early');
            this.toastCtrl.create({
              message: 'Mismatched '+ appointment.status == '4' ? "check out" : "check in",
              duration: 2000
            }).present();
          }
        }
      },
        (err) => {
          this.toastCtrl.create({
            message: 'something went wrong',
            duration: 2000
          }).present();
        });
    }
  }
  
  /**
   * Late Reasong popup
   */
  getReasonForLatePopup(scanResult: any, appointment: any, type: string) {
    var checkinType = appointment.status == '4' ? "check out" : "check in";
    var title = 'Give reason for ' + type + ' ' + checkinType;
    console.log(title)
    let checkinReasonPopup = this.modalCtrl.create('ModelPage', { title: title, appointment:appointment });
    checkinReasonPopup.onDidDismiss((reason) => {
      if (reason != null)
        this.executeCheckIn(scanResult, appointment, reason, type);
    });
    checkinReasonPopup.present();
  }

  /**
   * Execute checkin
   */
  async executeCheckIn(scanResult: any, appointment: any, reason: string, checkinType: any) {
    let appointmentIds = appointment.id.split('_');
    let response;
    if (appointment.status == '4') {
      let request = {
        Action: 'end',
        AppointmEntId: appointmentIds[appointmentIds.length - 1],
        EndTime: scanResult[3],
        CompanyId: appointment.companyid,
        ApptSource: appointment.apptsource
      };
      if (checkinType == "early")
        request['EarlyReason'] = reason;
      else if (checkinType == "late") {
        request['LateReason'] = reason;
      }

      response = await this.apiProvider.Post(AppConst.CHECK_INOUT, request).toPromise();
    }
    else {
      let request = {
        Action: 'start',
        AppointmEntId: scanResult[1],
        ProviderId: AppState.UserCred.userid,
        StartTime: scanResult[3],
        CompanyId: appointment.companyid,
        ApptSource: appointment.apptsource
      };
      if (checkinType == "early")
        request['EarlyReason'] = reason;
      else if (checkinType == "late") {
        request['LateReason'] = reason;
      }

      response = await this.apiProvider.Post(AppConst.CHECK_INOUT, request).toPromise();
    }

    if (response != null && response['status'])
      this.navCtrl.push('ProviderCheckinSuccessPage', appointment);
  }

next(el) {
  console.log('--------------------enterd otp --------------- >>>>>',this.digit1,this.digit2,this.digit3 ,this.digit4)
  console.log(this.mainOtp)
    el.setFocus();
  }



  /**
   * CheckIn
   * @param appointment 
   */
  async checkotp(appointment :any)   {
console.log('in appointments otp ',appointment)
    if (appointment != null) {
      if (appointment.status == "5" || appointment.status == "3" || appointment.status == "8") return;
console.log('------------main otp ')
    this.mainOtp=this.digit1+this.digit2+this.digit3+ this.digit4
    console.log(this.mainOtp)

    if(this.mainOtp!='NaN'){
    let appoitnmentId = this.appointment.id.split('_');

    if(this.appointment.memberdetails.mobilenumber !=''){
    var checkinRequest =
    {
        Action: "verifysmsotp",
        AppointmentId: appoitnmentId[appoitnmentId.length - 1],
        MemberId: AppState.UserCred.userid,
        userid:this.appointment.memberdetails.id,
        mobilenumber:this.appointment.memberdetails.mobilenumber,
        DateTime: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
        Type: this.appointment.status == "4" ? "checkout" : "checkin",
        CompanyId: this.appointment.companyid,
        ApptSource: this.appointment.apptsource,
        otp:this.mainOtp
    };

    let response =  await this.apiProvider.Post(AppConst.Get_QR_CODE, checkinRequest).toPromise();
    if (response != null) {
      console.log(response)
      for(let i in response)
      {
        console.log(response[i])
      }
// if(response['__zone_symbol__value']['codedetails'].status==true)
if(response['codedetails'].status == true )
{
  this.toastCtrl.create({
    message: 'OTP verified',
    duration: 2000
  }).present();

  response['codedetails'].text

  if (response['codedetails'].text != null) {
   console.log('-----------bar code scanner ------------- ',response['codedetails'].text)
    let appointmentIds = appointment.id.split('_');
    let scanResult = response['codedetails'].text.split('||');
    console.log(scanResult,appointmentIds)
    if (scanResult[1] == appointmentIds[appointmentIds.length - 1]) {
      //if (scanResult[scanResult.length - 1] == 'true') {              
      if (scanResult['5'] == 'true') {
        
        this.getReasonForLatePopup(scanResult, appointment, 'early');
      }
      //else if (scanResult[scanResult.length - 2] == 'true') {
      else if (scanResult['4'] == 'true') {
        this.getReasonForLatePopup(scanResult, appointment, 'late')
      }
      else {
        this.executeCheckIn(scanResult, appointment, null, null)
      }
    }
    else {
      console.log('in mismatch')
   //  this.getReasonForLatePopup(scanResult, appointment, 'early');
      this.toastCtrl.create({
        message: 'Mismatched '+ appointment.status == '4' ? "check out" : "check in",
        duration: 2000
      }).present();
    }
  }





}
     

console.log('otp varify for check in  ')

      // if (scanResult['5'] == 'true') {
              
      //   this.getReasonForLatePopup(scanResult, appointment, 'early');
      // }
      // //else if (scanResult[scanResult.length - 2] == 'true') {
      // else if (scanResult['4'] == 'true') {
      //   this.getReasonForLatePopup(scanResult, appointment, 'late')
      // }
      // else {
      //   this.executeCheckIn(scanResult, appointment, null, null)
      // }



     //   this.qrCode = response['qrcode'];
   //  this.checkForAppointmentStatus()
    
    this.toastCtrl.create({
      message: 'Please enter correct OTP',
      duration: 2000
    }).present();
  }
}else{
    this.toastCtrl.create({
      message: 'Customer mobile is not verified',
      duration: 2000
    }).present();
  }
}else{
  this.toastCtrl.create({
    message: 'Please enter SMS code.',
    duration: 2000
  }).present();

}

    }

    
  }


   /**
     * Generate  code
     */
    async generateSMSCode() {
      this.sendSMs=true
      let appoitnmentId = this.appointment.id.split('_');

      if(this.appointment.memberdetails.mobilenumber !=''){
      var checkinRequest =
      {
          Action: "generatesmsotp",
          AppointmentId: appoitnmentId[appoitnmentId.length - 1],
          MemberId: AppState.UserCred.userid,
          userid:this.appointment.memberdetails.id,
          mobilenumber:this.appointment.memberdetails.mobilenumber,
          DateTime: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
          Type: this.appointment.status == "4" ? "checkout" : "checkin",
          CompanyId: this.appointment.companyid,
          ApptSource: this.appointment.apptsource
      };

      let response = await this.apiProvider.Post(AppConst.Get_QR_CODE, checkinRequest).toPromise();
      if (response != null && response['status']) {


       //   this.qrCode = response['qrcode'];
     //  this.checkForAppointmentStatus()
      }
    }else{
      this.toastCtrl.create({
        message: 'Customer mobile is not verified',
        duration: 2000
      }).present();
    }
  }

  submit()
  {

  }
  // sendOtp()
  // {
   
    
  // }
}
