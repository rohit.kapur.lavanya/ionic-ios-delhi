import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '../../../node_modules/@ionic-native/in-app-browser';
import { from } from 'rxjs/observable/from';
import { ApiProvider } from '../../providers/api/api';


/**
 * Generated class for the SumupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sumup',
  templateUrl: 'sumup.html',
})
export class SumupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public SumupApi :InAppBrowser, private apiProvider:ApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SumupPage');
  }


   async SumupAuth()
  {
//https://api.sumup.com/v0.1/checkouts

let request=
  {
    "checkout_reference": "string",
    "amount": 0,
    "currency": "EUR",
    "pay_to_email": "user@example.com",
    "pay_from_email": "user@example.com",
    "description": "string",
    "return_url": "http://example.com"
  
}
var response = await this.apiProvider.Post('https://api.sumup.com/v0.1/checkouts', request).toPromise();


  }
  //   plugins.sumup.pay(
  //     function(res) {
  //       /*
  //       res : {
  //           code // result code from sumup, more info here : https://github.com/sumup/sumup-android-sdk#1-response-fields
  //           message // message from sumup
  //           txcode // transaction code from sumup
  //         }
  //       */
  //     },
  //     function(error) {
  
  //     }, '10', 'EUR' , 'raj.ammy99@gmail.com', '789634');
  
  // //  GET https://api.sumup.com/authorize?response_type=code&client_id=fOcmczrYtYMJ7Li5GjMLLcUeC9dN&redirect_uri=https://sample-app.example.com/callback&scope=payments%20user.app-settings%20transactions.history%20user.profile_readonly&state=2cFCsY36y95lFHk4
  //   var web=this.SumupApi.create('https://www.google.com/maps/search/?api=1&query='+this.latitude+','+this.longitude+' ');
  // }
  
}
