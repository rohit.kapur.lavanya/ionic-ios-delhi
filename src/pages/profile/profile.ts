import { NgZone } from '@angular/core';
import { IonicPage, Slides, NavController, LoadingController, AlertController, ToastController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';
import { AppConst } from '../../AppConst';
import { Component, ViewChild, } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import "rxjs/add/operator/filter";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AppState } from '../../AppStates';
import { Base64Binary } from "js-base64";
import { Base64 } from "@ionic-native/base64";
//import { ProfileEditPage } from '../profile-edit/profile-edit';
import { DatePipe } from '@angular/common';
//import { ProviderProfileEditPage } from '../provider-profile-edit/provider-profile-edit';
import { FileChooser } from '@ionic-native/file-chooser';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
//import { File } from '@ionic-native/file';

import { UpdateprofilePage } from '../updateprofile/updateprofile';
import { updateDate } from '../../../node_modules/ionic-angular/umd/util/datetime-util';
import { UpdateMemberProfilePage } from '../update-member-profile/update-member-profile';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { apps } from 'firebase';
import { Helper } from '../../helpers/helper';

import * as moment from "moment";
//import { UpdateprofiletwoPage } from '../updateprofiletwo/updateprofiletwo';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  @ViewChild(Slides) slides: Slides;
  slideOneForm: FormGroup;
  slideTwoForm: FormGroup;

  submitAttempt: boolean = false;

  bottomPad = '10px';
  showPass: boolean = false;
  showConfirmPass: boolean = false;
  email: string;
  password: string = '';
  confirmPassword: string = '';
  currentUser: any;
  userInfo;
  formFields: any;
  values: {} = {};
  dob: any;
  providerTypes: any;
  services: any;
  skills: any;
  interests: any;
  companyId: any;
  providerskills: string[];
  providerinterests: string[];
  base64Image: string;
  newimage: string;
  public apstate = AppState
  filenameExtension: string[];
  DotExE: string[];
  fileEXE: string[];
  filexe: any;
  setextension: any;
  formatename: string;
  ResCode: number;
  uplodedata = [];
  Submit: boolean = false;
  ///////////////////////////
  ProfilePic: string;
  DisplayName: string;
  Gender: string;
  Dob: string;
  EmailId: string;
  AddressLine1: string;
  AddressLine2: string;
  Mobile: string;
  SelectedTab: any
  CompatibilityTab:any
  // base64Image: string;
  user: any;
  // providerskills:any;
  public profile_completion_percentage = [];
  EmergencyData: any;
  Recrumentdata: any;
  RecrumentDocument: any;
  PropertyListdata: any;
  PROVIDERCANDIDATELIST: any;
  ProvioderClientdata: any;
  Reviewdata: any;
  Companyid: any;
  ProviderId: any;
  CompanyClinicId: any;
  ClinicAddressid: any;
  provider: any;
  availabilities: any;
  data: any;
  monthHeader: any;
  currentNavigatedDateMonth: Date;
  currentNavigatedDateWeek: Date;
  currentDate = new Date();
  currentMonthDates: any[];
  row: number;
  column: any;
  weekDates: any[];
  providerCyclesData: Object;
  providerCycleId: any;
  loader: any;
  fieldName: string;
  appointmentsTab: boolean = false;
  appointmentsStartDate: any = '';
  appointmentsType: string;
  appointmentService: any;
  appointmentTime: any;
  appointmentMemberName: any;
  appointmentLocation: string;
  appointmentsSelectedDate: any;
  constructor(
    private fileChooser: FileChooser,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    public zone: NgZone,
    private events: Events,
    public alertController: AlertController,
    public apiprovider: ApiProvider,
    private camera: Camera,
    private base64: Base64,
    private datePipe: DatePipe,
    private apiProvider: ApiProvider,
    private toastCtrl: ToastController,
    private storage: Storage,
    private loadingController: LoadingController,
    private transfer: FileTransfer,
    private loadingcontroller: LoadingController
  ) {

    this.currentNavigatedDateMonth = this.currentDate;
    this.currentNavigatedDateWeek = this.getCurrentNavigatedDateWeek();
    this.currentNavigatedDateMonth = this.currentDate;

    this.bottomPad = AppState.IsDashboard ? '55px' : '5px';
    this.currentUser = AppState.UserCred;
    console.log(this.currentUser)
    this.userInfo = this.currentUser.formvalues;
    console.log(this.userInfo);
    this.companyId = this.currentUser.currentCompany.companyid;
    this.dob = this.userInfo.dateofbirth;
    this.dob = this.userInfo.dateofbirth;
    if (this.userInfo.dateofbirth != '') {
      let dateArray = this.userInfo.dateofbirth.split("-");
      if (dateArray[0].length < 4) {
        this.dob = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
      }
    }


    this.providerskills = AppState.UserCred.providerskillsid;
    this.providerinterests = AppState.UserCred.providerinterestsid;



    console.log("providerskills=" + this.providerskills);

    this.getEditFields();
    this.getProviderTypes();
    this.getSkills();
    this.getInterests();
    this.getEmergencyContact();

    console.log(AppState.UserCred)
    this.user = AppState.UserCred;
    this.profile_completion_percentage.push({ 'company_information': 100 }, { 'personal_information': 90 }, { 'qualification_information': 60 });

    this.ProfilePic = AppState.UserCred['avatar'] == null || AppState.UserCred['avatar'] == '' ? "assets/imgs/userred.png" : AppState.UserCred['avatar'];

    var usertitle = AppState.UserCred['formvalues'].hasOwnProperty('title') ? AppState.UserCred['formvalues']['title'] : '';

    this.DisplayName = usertitle + ' ' + AppState.UserCred['firstname'] + ' ' + AppState.UserCred['lastname'];
    var gender = AppState.UserCred['formvalues']['gender'];

    if (gender != undefined) {
      this.Gender = gender.charAt(0).toUpperCase() + gender.slice(1, gender.length);
    }
    // this.Dob = AppState.UserCred['formvalues']['dateofbirth'];
    var userdateofbirth = AppState.UserCred['formvalues'].hasOwnProperty('dateofbirth') ? AppState.UserCred['formvalues']['dateofbirth'] : '';
    if (userdateofbirth != '') {
      let dateArray = userdateofbirth.split("-");
      if (dateArray[0].length < 4) {
        let formatedDate = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];
        this.Dob = formatedDate;
        // this.Dob = this.datePipe.transform(formatedDate, 'dd-MM-yyyy');
      } else {
        this.Dob = userdateofbirth;
        // this.Dob = this.datePipe.transform(userdateofbirth, 'dd-MM-yyyy');
      }
    }
    console.log('---------------------Date of birth----------------------');
    console.log(this.Dob);
    console.log('---------------------Date of birth----------------------');



    this.EmailId = AppState.UserCred['emailid'];


    //this.AddressLine1 = AppState.UserCred['formvalues']['addressline'];

    this.AddressLine1 = AppState.UserCred['formvalues'].hasOwnProperty('addressline') ? AppState.UserCred['formvalues']['addressline'] : '';


    var usermobilenumber = AppState.UserCred['formvalues'].hasOwnProperty('mobilenumber') ? AppState.UserCred['formvalues']['mobilenumber'] : '';

    var userphonenumber = AppState.UserCred['formvalues'].hasOwnProperty('phonenumber') ? AppState.UserCred['formvalues']['phonenumber'] : '';

    this.AddressLine2 = ((AppState.UserCred['formvalues']['addressline1'] != 'undefined' && AppState.UserCred['formvalues']['addressline1'] != null) ? AppState.UserCred['formvalues']['addressline1'] : '' + ',') + AppState.UserCred['formvalues']['city'];
    this.Mobile = AppState.IsMember ? usermobilenumber : userphonenumber;

    this.providerskills = AppState.UserCred['providerskills'];


console.log('apstate : ', this.apstate);
    if (this.apstate.IsMember) {
      this.fieldName = 'memberid';
    } else {
      this.fieldName = 'providerid';
    }
  }

  ionViewDidLoad() {

    this.SelectedTab = '1'
    this.CompatibilityTab="11"
    console.log('ionViewDidLoad UpdateprofilePage');
    this.getEditFields();
    this.getProviderTypes();
    this.getSkills();
    this.getInterests();
    this.GetDocs()
    this.ProviderCandidate();
    this.ProviderClient();
    this.ReviewList();
    this.profile_completion_percentage.push({ 'company_information': 100 }, { 'personal_information': 90 }, { 'qualification_information': 60 });

    //for the avalability

    this.getProviderCycles();
    this.getWeekDays();
    this.getClinicListpage();
    setTimeout(x => {
      this.getMonthDates();
    this.getSavedAvailabilities();
      }, 1000);


  }
  /**
     * Get image from gallery
     */
  getImageFromGallery() {
    let cameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      //destinationType:this.camera.DestinationType.DATA_URL,
      quality: 100,
      targetWidth: 1000,
      targetHeight: 1000,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true
    }

    this.camera.getPicture(cameraOptions)
      .then((file_uri) => {
        this.base64Image = file_uri;
        this.uploadPhoto();
      },
        (err) => {
          console.log(err);
        });
  }



  /**
   * Get picture from camera
   */
  getImageFromCamera() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64: => 'data:image/jpeg;base64,'

      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      if (this.base64Image) {
        this.uploadPhoto();
      }

    }, (err) => {
      console.log(err);
    });
  }

  cycleSegmentChanged(event){
    console.log('inside the segmentChanged()');
    console.log('cycle changed with the event : ', event.value);
    this.providerCycleId = event.value;
    this.loader = this.loadingcontroller.create({
      content: "please wait.."
    })
    this.loader.present()

    //clear the previous data
    if (document.querySelectorAll('.provider-cycle-saved-availability').length > 0) {
      Array.from(document.querySelectorAll('.provider-cycle-saved-availability')).forEach((a) =>{
        a.remove();
      });
    }

    this.getSavedAvailabilities();


  // console.log('CompatibilityTab',this.CompatibilityTab,event)
  }

  async getProviderCycles(){
    var filters = [
      {fieldname: "providerid", fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];
    const request =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

      };

    const response = await this.apiProvider.Post(AppConst.PROVIDERCYCLELIST, request).toPromise();
    if (response != null && response.hasOwnProperty('records') && response['records'].length > 0) {

      this.providerCyclesData = response;
    }
  }

  /**
   * Get week days
   */
  async getWeekDays() {
    console.log('innew week data')
    var dates = [];

    for (let i = 0; i <= 6; i++) {
      dates.push(new Date(this.currentNavigatedDateWeek.getTime() + i * 24 * 60 * 60 * 1000).getDate());
    }
    console.log(dates)
  this.weekDates=dates;
  }

    /**
   * Get current date of the week
   */
  getCurrentNavigatedDateWeek() {
    let difference = this.currentDate.getDay() - 1;
    if (difference < 0)
      difference += 7;
    return new Date(this.currentDate.getTime() + (-1 * difference * 24 * 60 * 60 * 1000));
  }

  async getClinicListpage() {
    var filters = [
      {fieldname: "provider_id", fieldvalue: "362", operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];
    var request =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "DESC" },
        companyid: this.Companyid,
        companyno: "",
        login_Userid: this.ProviderId,
        sourceapi: "wemalife"
      //  MemberId: AppState.UserCred.userid,

      };
    var response = await this.apiProvider.Post(AppConst.clinic, request).toPromise();
    console.log('inside the getClinbicListPage  response:', response)

    console.log('inside the getClinbicListPage  this.data:', this.data)
    }

     /**
   * Get month view dates
   */
  async getMonthDates() {
    //  this.monthHeader = Helper.getFullMonth(this.currentNavigatedDateMonth.getMonth()) + ' ' + this.currentNavigatedDateMonth.getFullYear();
    this.monthHeader = Helper.getFullMonth(this.currentNavigatedDateMonth.getMonth());
    let currentMonthNumberOfDays = new Date(this.currentNavigatedDateMonth.getFullYear(), this.currentNavigatedDateMonth.getMonth() + 1, 0).getDate();
      this.currentMonthDates = [];
      for (let i = 1; i <= currentMonthNumberOfDays; i++) {
        this.currentMonthDates.push(new Date(this.currentNavigatedDateMonth.getFullYear(), this.currentNavigatedDateMonth.getMonth(), i));
      }
      let firstDay = this.currentMonthDates['0'].getDay() + 1;
      this.row = 0;
      this.column = firstDay;
      //Clear month divs
      let items = document.getElementsByClassName('monthDiv');
      for (let i = 0; i < items.length; i++) {
        let item = <HTMLDivElement>items.item(i);
        //item.style.height = '30px';
        item.innerHTML = '';
      }

      // this.currentMonthDates.forEach(element => {
      //   this.setMonthDivs(element, currentMonthNumberOfDays);
      // });
    }

    showAppointmentPopup(appointments){
      if(appointments['memberdetails'] != undefined){
        console.log('showAppointmentPopup and show the appointments of : ', appointments);
        this.appointmentService = appointments['service'];
        //moment(response['records'][4]['start'], 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss'));
        this.appointmentTime = moment(appointments['start'], 'YYYY-MM-DDTHH:mm:ss').format('HH:mm') + ' - ' +  moment(appointments['end'], 'YYYY-MM-DDTHH:mm:ss').format('HH:mm');
        this.appointmentMemberName = appointments['memberdetails']['firstname'];
        this.appointmentLocation = appointments['memberdetails']['addressline'] + ', ' + appointments['memberdetails']['city'] + ', ' + appointments['memberdetails']['county'] + ', ' + appointments['memberdetails']['postcode'];

        var choosePopup = this.alertController.create({
          cssClass: 'appointmentPopup',
          subTitle: `<div> Services : ${this.appointmentService} </div>
                    <br> <div> Time : ${this.appointmentTime} </div>
                    <br> <div> Member Name : ${this.appointmentMemberName} </div>
                    <br> <div> Location : ${this.appointmentLocation} </div>`,
          buttons: [
            {
              text: "Close",
              cssClass: "close-button",
              handler: data => {
                choosePopup.dismiss();
              }
            },
          ]
        });
        choosePopup.present();
      }
    }

    appointmentsTabSegment(event){
      // console.log('event  :', event.value);
        //clear the previous data
      let date;
      if (document.querySelectorAll('.provider-cycle-saved-availability').length > 0) {
        Array.from(document.querySelectorAll('.provider-cycle-saved-availability')).forEach((a) =>{
          a.parentElement.removeAttribute('clickflag');
          a.remove();
        });
      }
      this.appointmentsType = event.value;
      if (this.appointmentsSelectedDate != '') {
        date = this.appointmentsSelectedDate;
      }else{
        date = ''; 
      }

      console.log('inside the appointmentsTabSegment : ', date);
      this.getSavedAppointments(date, event.value);
    }

    appointmentDateChange(date){
      // console.log('date change', date);
        //clear the previous data
      if (document.querySelectorAll('.provider-cycle-saved-availability').length > 0) {
        Array.from(document.querySelectorAll('.provider-cycle-saved-availability')).forEach((a) =>{
          a.parentElement.removeAttribute('clickflag');
          a.remove();
        });
      }
      this.appointmentsSelectedDate = date;
      this.getSavedAppointments(date);
    }

  async getSavedAppointments(date = '', type = ''){

    this.appointmentsTab = true;

    if (type != '') {
      this.appointmentsType = type;
    } else {
      this.appointmentsType = 'diary';
    }
    
    var startdate, enddate;
    if(date != ''){

      startdate = new Date(date);
      startdate.setDate(startdate.getDate() + (1 - 7 - startdate.getDay()) % 7);
      startdate = startdate.getFullYear() + "-" + ("0"+(startdate.getMonth()+1)).slice(-2) + "-"  + ("0" + startdate.getDate()).slice(-2);

      enddate = new Date(date);
      enddate.setDate(enddate.getDate() + (1 + 7 - enddate.getDay()) % 7);
      enddate = enddate.getFullYear() + "-" + ("0"+(enddate.getMonth()+1)).slice(-2) + "-"  + ("0" + enddate.getDate()).slice(-2);
    
    }else{
      startdate = ( this.currentNavigatedDateWeek.getFullYear() + "-" + ("0"+(this.currentNavigatedDateWeek.getMonth()+1)).slice(-2) + "-"  + ("0" + this.currentNavigatedDateWeek.getDate()).slice(-2) );
      enddate = new Date(this.currentNavigatedDateWeek.getTime() + 7 * 24 * 60 * 60 * 1000);
      enddate = enddate.getFullYear() + "-" + ("0"+(enddate.getMonth()+1)).slice(-2) + "-"  + ("0" + enddate.getDate()).slice(-2);
    }

    if (startdate == enddate) {
      enddate = new Date(enddate);
      enddate = new Date(enddate.setTime(enddate.getTime() + (7 * 24 * 60 * 60 * 1000)));
      enddate = enddate.getFullYear() + "-" + ("0"+(enddate.getMonth()+1)).slice(-2) + "-"  + ("0" + enddate.getDate()).slice(-2);
    }
    console.log('startdate : ', startdate);
    console.log('appointmentsType : ', this.appointmentsType);
    


    let response;
    if (this.apstate.IsMember) {
      const request =
      {
        'memberid': AppState.UserCred.userid,
        startdate : startdate,
        enddate: enddate,
      };

      response = await this.apiProvider.Post(AppConst.GETMEMBERAPPOINTMENTS, request).toPromise();
      console.log('GETMEMBERAPPOINTMENTS : ', response);
    }else{
      
    var request = {
      companyid: AppState.UserCred.currentCompany.companyid,
      providerid: AppState.UserCred.userid,
      startdate : startdate,
      enddate: enddate,
    };

    console.log('inside getSavedAppointments() request : ', request);
    response = await this.apiProvider.Post(AppConst.GET_APPOINTMENTS, request).toPromise();
    console.log('inside getSavedAppointments() response : ', response);

    }
    if(response['records'] != null && response['records'].length > 0){


      //proceed
      // console.log('date : ',moment(response['records'][4]['start'], 'YYYY-MM-DDTHH:mm:ss').format('YYYY-MM-DD'));
      // console.log('time : ',moment(response['records'][4]['start'], 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss'));
      // console.log('dayofweek : ',moment(response['records'][4]['start'], 'YYYY-MM-DDTHH:mm:ss').isoWeekday().toString());

      let elements = document.getElementsByClassName('weekDiv');
          for (let i = 0; i < elements.length; i++) {
            let element = <HTMLDivElement>elements.item(i);
            let id = element.id.split('_');
            let items = response['records'].filter(x => moment(x.start, 'YYYY-MM-DDTHH:mm:ss').isoWeekday().toString() == id[3]);
            if (items != null && items.length > 0) {
              items.forEach(appointments => {
                appointments.starttime = moment(appointments.start, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss');
                appointments.endtime = moment(appointments.end, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss');
                appointments.dayofweek = moment(appointments.start, 'YYYY-MM-DDTHH:mm:ss').isoWeekday().toString();
                // console.log('????????????????????????????????????????????????????????????????????????????????????????????????????');
                // console.log('appointments : start  :', appointments.start);
                // console.log('appointments : dayofweek  :', moment(appointments.start, 'YYYY-MM-DDTHH:mm:ss').isoWeekday().toString());
                // console.log('????????????????????????????????????????????????????????????????????????????????????????????????????');

                var timeStart = new Date("01/01/2007 " + appointments.start).getHours();
                var timeEnd = new Date("01/01/2007 " + appointments.end).getHours();
                var hourDiff = timeEnd - timeStart;
                var startTimes = moment(appointments.start, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss').split(':');
                var endTimes = moment(appointments.end, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss').split(':');


                // console.log('inside item foreach availability');
                //console.log(id)

                //previous candition :commented by nsp
               // if (parseInt(id[1]) < parseInt(endTimes[0]) && id[3] == availability.dayofweek && element.getAttribute('clickFlag') == null) {

                  //current candition :added by nsp

                  if(appointments.dayofweek == '5'){
                    // console.log("all out ----- calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                  }

                  // console.log('***************************StartBigIf********************************************');
                  // console.log('parseInt(id[1]) <= parseInt(endTimes[0] : ', parseInt(id[1]) <= parseInt(endTimes[0]));
                  // console.log('(( (parseInt(id[1]) >= parseInt(startTimes[0]) ) && id[3] == appointments.dayofweek )) : ', (
                  //   ( (parseInt(id[1]) >= parseInt(startTimes[0]) ) && id[3] == appointments.dayofweek )
                  //   ));

                  // console.log('id[3] == appointments.dayofweek : ', id[3] == appointments.dayofweek);
                  // console.log('element : ', element);




                  // console.log('***************************EndBigIf********************************************');
                  if (  (parseInt(id[1]) <= parseInt(endTimes[0]) )    &&

                  (
                    ( (parseInt(id[1]) >= parseInt(startTimes[0]) ) && id[3] == appointments.dayofweek )
                    )

                    // && id[3] == appointments.dayofweek && element.getAttribute('clickFlag') == null) {
                    && id[3] == appointments.dayofweek) {


                      // console.log('inside big if condition');


                      if(appointments.dayofweek == '5'){
                        // console.log("outer----- calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                      }
                      if ( id[1] !='' && parseInt(startTimes[1]) === parseInt('30') && parseInt(id[2]) != parseInt(startTimes[1]) && parseInt(id[1]) ===  parseInt(startTimes[0])) {

                        if(appointments.dayofweek == '5'){
                          // console.log("iffff calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                        }

                      }else if ( parseInt(endTimes[1]) === parseInt('30')  && parseInt(id[1]) === parseInt(endTimes[0])  ) {


                        if(parseInt(id[2]) === parseInt(endTimes[1])){

                        }else {
                          let innerDiv = document.createElement('div');
                          innerDiv.style.height = '30px';
                          // console.log(appointments);
                          innerDiv.setAttribute('class', 'provider-cycle-saved-availability');
                          if(appointments.memberdetails != undefined || appointments.memberdetails != null){
                            // if (appointments.appointment_type == 'diary' && this.appointmentsType =='diary') {
                            //   //show all the times
                            //   console.log('show all the appointment time : ', appointments);
                            // } else if(appointments.appointment_type == 'regular' && this.appointmentsType =='regular') {
                            //   //only show the regular appointments time
                            //   console.log('only show the regular appointments time : ', appointments);
                            // }

                            if (this.appointmentsType =='regular') {
                              if (appointments.appointment_type == 'regular') {
                                innerDiv.innerHTML = moment(appointments['starttime'], 'HH:mm:ss').format('HH:mm') + ' - ' + moment(appointments['endtime'], 'HH:mm:ss').format('HH:mm');                                
                              }
                            } else {
                              innerDiv.innerHTML = moment(appointments['starttime'], 'HH:mm:ss').format('HH:mm') + ' - ' + moment(appointments['endtime'], 'HH:mm:ss').format('HH:mm'); 
                            }


                            innerDiv.addEventListener('click', () => {
                            this.showAppointmentPopup(appointments);

                            // console.log('clicked!');
                            });
                            innerDiv.setAttribute('clickFlag', 'true');

                            innerDiv.style.background = '#319c95';
                          }else{
                            innerDiv.style.background = appointments.hasOwnProperty('status') && appointments.status == '3' ? '#E74C3C' : '#d2eeec';
                          }
                          // console.log(innerDiv)
                          // element.addEventListener('click', () => {
                          // // this.editappointmentsPopup(appointments);
                          // this.showAppointmentPopup(appointments);

                          // // console.log('clicked!');
                          // });
                          // element.setAttribute('clickFlag', 'true');
                          element.appendChild(innerDiv);
                        }

                        if(appointments.dayofweek == '5'){
                          // console.log("el--- calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                        }



                      }  else{

                        if ( parseInt(endTimes[1]) === parseInt('0')  && parseInt(id[1]) === parseInt(endTimes[0])  ) {

                        }else{



                        if(appointments.dayofweek == '5'){
                          // console.log("elssssee new calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                        }

                        let innerDiv = document.createElement('div');
                        innerDiv.style.height = '30px';
                        // console.log(appointments);
                        innerDiv.setAttribute('class', 'provider-cycle-saved-availability');
                        if(appointments.memberdetails != undefined || appointments.memberdetails != null){

                          if (this.appointmentsType =='regular') {
                            if (appointments.appointment_type == 'regular') {
                              innerDiv.innerHTML = moment(appointments['starttime'], 'HH:mm:ss').format('HH:mm') + ' - ' + moment(appointments['endtime'], 'HH:mm:ss').format('HH:mm');                                
                            }
                          } else {
                            innerDiv.innerHTML = moment(appointments['starttime'], 'HH:mm:ss').format('HH:mm') + ' - ' + moment(appointments['endtime'], 'HH:mm:ss').format('HH:mm'); 
                          }

                          innerDiv.addEventListener('click', () => {
                            this.showAppointmentPopup(appointments);

                            // console.log('clicked!');
                            });
                        innerDiv.setAttribute('clickFlag', 'true');



                          innerDiv.style.background = '#319c95';
                          // innerDiv.innerHTML = moment(appointments['starttime'], 'HH:mm:ss').format('HH:mm') + ' - ' + moment(appointments['endtime'], 'HH:mm:ss').format('HH:mm');
                        }else{
                          innerDiv.style.background = appointments.hasOwnProperty('status') && appointments.status == '3' ? '#E74C3C' : '#d2eeec';
                        }
                        // innerDiv.style.background = appointments.hasOwnProperty('status') && appointments.status == '3' ? '#E74C3C' : '#d2eeec';
                        // console.log(innerDiv)
                        // element.addEventListener('click', () => {
                        // this.showAppointmentPopup(appointments);
                        // // console.log('clicked!');
                        // });
                        // element.setAttribute('clickFlag', 'true');
                        element.appendChild(innerDiv);
                      }
                    }

                }
                else {
                  let exist = false;
                  items.forEach(item => {


                    item.starttime = moment(item.start, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss');
                    item.endtime = moment(item.end, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss');

                    if (parseInt(id[1]) >= parseInt(item.starttime.split(':')[0]) && parseInt(id[1]) < parseInt(item.endtime.split(':')[0]))
                      exist = true;
                  });
                  if (exist == false && element.getAttribute('clickFlag') == null) {

               //Commented by : nsp -  previous code start from here
                    /*element.setAttribute('clickFlag', 'true');    //Commented by mag (nsp to fill enddate half hour )
                    let param = id[1] + '_' + (id[2] == '0' ? id[2] + '0' : id[2]) + '_' + (id[3] == '0' ? '7' : id[3]);
                    element.addEventListener('click', () => {
                      this.showAvailabilityPopup(param);
                    });*/
               //Commented by : nsp -  previous code end from here


               //additional code added  by : nsp -  start here
                   let callclickFlag = false;
                    if ( id[1] !='' && parseInt(startTimes[1]) === parseInt('30') && parseInt(id[2]) != parseInt(startTimes[1]) && parseInt(id[1]) ===  parseInt(startTimes[0])) {

                      callclickFlag = true;
                      element.setAttribute('clickFlag', 'true');    //Commented by mag (nsp to fill enddate half hour )

                    }else if ( parseInt(endTimes[1]) === parseInt('30')  && parseInt(id[1]) === parseInt(endTimes[0])  ) {
                      callclickFlag = true;

                      }
                  if(callclickFlag){
                    element.setAttribute('clickFlag', 'true');
                    let param = id[1] + '_' + (id[2] == '0' ? id[2] + '0' : id[2]) + '_' + (id[3] == '0' ? '7' : id[3]);
                    // element.addEventListener('click', () => {
                    //   this.showAvailabilityPopup(param);
                    // });
                  }
                //additional code added  by : nsp -  end here


                  }
                }
              });

              // console.log("response['records'] : ", response['records']);

              // response['records'].forEach(record => {
              //   if (record['memberdetails'] != undefined) {
              //     //member details exists
              //     console.log('record : ', record);
              //     // console.log('App_' + record['starttime'].split(':')[0] + '_' +  record['starttime'].split(':')[1] + '_' + moment(record['starttime'], 'YYYY-MM-DDTHH:mm:ss').isoWeekday().toString());
              //   }
              // });
            }//end if
          }// end for
    }
  }


    async getSavedAvailabilities(){
      this.appointmentsTab = false;
      this.loader = this.loadingcontroller.create({
        content: "please wait.."
      })
      this.loader.present();

      let filters = [
        { fieldname: "providerid", fieldvalue: AppState.UserCred.userid, operators: "Equal" },
      ];
      var request = {
        filter: filters,
        filterproperty: { offset: 0, orderby: "id", recordlimit: 500 ,dir: "DESC"},
        sourceapi: 'wemalife',
        login_Userid: AppState.UserCred.userid,
        companyid: AppState.UserCred.currentCompany.companyid,
        // weekdate : this.currentNavigatedDateWeek.getFullYear() + '-'+ this.currentNavigatedDateWeek.getMonth() + '-' + this.currentNavigatedDateWeek.getDate()
        weekdate : ( this.currentNavigatedDateWeek.getFullYear() + "-" + ("0"+(this.currentNavigatedDateWeek.getMonth()+1)).slice(-2) + "-"  + ("0" + this.currentNavigatedDateWeek.getDate()).slice(-2) )

      };

      console.log('get the week date : ', this.weekDates);

      console.log('availability request : ', request);

      let response = await this.apiProvider.Post(AppConst.PROVIDERAVAILABILITYWEEKLY, request).toPromise();

      console.log('availability response : ', response);

      // if (response != null && response.hasOwnProperty('records') && response['records'].length > 0) {
      if (response != null && response['availability'].length > 0) {

      console.log('savedAVALABILITY : ', response);
      // this.provider = response['records'][0];
      // this.Companyid= response['records'][0].companyid;
      // this.ProviderId=response['records'][0].userid;
      // this.CompanyClinicId= response['records'][0].company_clinic_id;
      // this.ClinicAddressid= response['records'][0].company_clinic_id
      this.provider = response;
      if (this.provider != null) {
        console.log('inside the provider');
        if (this.provider.availability != null && this.provider.availability.length > 0) {
          console.log('response has availability');
          this.availabilities = this.provider.availability.filter(x => x.companyid == AppState.UserCred.currentCompanyId);
          this.availabilities.forEach(availability => {
          console.log('availabilities forEach availability');

          let elements = document.getElementsByClassName('weekDiv');
          for (let i = 0; i < elements.length; i++) {
            let element = <HTMLDivElement>elements.item(i);
            let id = element.id.split('_');
            let items = this.availabilities.filter(x => x.dayofweek == id[3]);
            if (items != null && items.length > 0) {
              items.forEach(availability => {
                var timeStart = new Date("01/01/2007 " + availability.starttime).getHours();
                var timeEnd = new Date("01/01/2007 " + availability.endtime).getHours();
                var hourDiff = timeEnd - timeStart;
                var startTimes = availability.starttime.split(':');
                var endTimes = availability.endtime.split(':');

                console.log('inside item foreach availability');
                //console.log(id)

                //previous candition :commented by nsp
               // if (parseInt(id[1]) < parseInt(endTimes[0]) && id[3] == availability.dayofweek && element.getAttribute('clickFlag') == null) {

                  //current candition :added by nsp

                  if(availability.dayofweek == '5'){
                    console.log("all out ----- calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                  }

                  console.log('***************************StartBigIf********************************************');
                  console.log('parseInt(id[1]) <= parseInt(endTimes[0] : ', parseInt(id[1]) <= parseInt(endTimes[0]));
                  console.log('(( (parseInt(id[1]) >= parseInt(startTimes[0]) ) && id[3] == availability.dayofweek )) : ', (
                    ( (parseInt(id[1]) >= parseInt(startTimes[0]) ) && id[3] == availability.dayofweek )
                    ));

                  console.log('id[3] == availability.dayofweek : ', id[3] == availability.dayofweek);
                  console.log('element : ', element);




                  console.log('***************************EndBigIf********************************************');
                  if (  (parseInt(id[1]) <= parseInt(endTimes[0]) )    &&

                  (
                    ( (parseInt(id[1]) >= parseInt(startTimes[0]) ) && id[3] == availability.dayofweek )
                    )

                    && id[3] == availability.dayofweek && element.getAttribute('clickFlag') == null) {
                    // && id[3] == availability.dayofweek) {


                      console.log('inside big if condition');


                      if(availability.dayofweek == '5'){
                        console.log("outer----- calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                      }
                      if ( id[1] !='' && parseInt(startTimes[1]) === parseInt('30') && parseInt(id[2]) != parseInt(startTimes[1]) && parseInt(id[1]) ===  parseInt(startTimes[0])) {

                        if(availability.dayofweek == '5'){
                          console.log("iffff calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                        }

                      }else if ( parseInt(endTimes[1]) === parseInt('30')  && parseInt(id[1]) === parseInt(endTimes[0])  ) {


                        if(parseInt(id[2]) === parseInt(endTimes[1])){

                        }else {
                          let innerDiv = document.createElement('div');
                          innerDiv.style.height = '30px';
                          console.log(availability);
                          innerDiv.setAttribute('class', 'provider-cycle-saved-availability');
                          innerDiv.style.background = availability.hasOwnProperty('status') && availability.status == '3' ? '#E74C3C' : '#d2eeec';
                          console.log(innerDiv)
                          // element.addEventListener('click', () => {
                          // this.editAvailabilityPopup(availability);
                          // });
                          // element.setAttribute('clickFlag', 'true');
                          element.appendChild(innerDiv);
                        }

                        if(availability.dayofweek == '5'){
                          console.log("el--- calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                        }



                      }  else{

                        if ( parseInt(endTimes[1]) === parseInt('0')  && parseInt(id[1]) === parseInt(endTimes[0])  ) {

                        }else{



                        if(availability.dayofweek == '5'){
                          console.log("elssssee new calendar data =  startTimes-"+startTimes+", endTimes-"+endTimes +", id-"+id)
                        }

                        let innerDiv = document.createElement('div');
                        innerDiv.style.height = '30px';
                        console.log(availability);
                        innerDiv.setAttribute('class', 'provider-cycle-saved-availability');
                        innerDiv.style.background = availability.hasOwnProperty('status') && availability.status == '3' ? '#E74C3C' : '#d2eeec';
                        console.log(innerDiv)
                        // element.addEventListener('click', () => {
                        // this.editAvailabilityPopup(availability);
                        // });
                        // element.setAttribute('clickFlag', 'true');
                        element.appendChild(innerDiv);
                      }
                    }

                }
                else {
                  let exist = false;
                  items.forEach(item => {
                    if (parseInt(id[1]) >= parseInt(item.starttime.split(':')[0]) && parseInt(id[1]) < parseInt(item.endtime.split(':')[0]))
                      exist = true;
                  });
                  if (exist == false && element.getAttribute('clickFlag') == null) {

               //Commented by : nsp -  previous code start from here
                    /*element.setAttribute('clickFlag', 'true');    //Commented by mag (nsp to fill enddate half hour )
                    let param = id[1] + '_' + (id[2] == '0' ? id[2] + '0' : id[2]) + '_' + (id[3] == '0' ? '7' : id[3]);
                    element.addEventListener('click', () => {
                      this.showAvailabilityPopup(param);
                    });*/
               //Commented by : nsp -  previous code end from here


               //additional code added  by : nsp -  start here
                   let callclickFlag = false;
                    if ( id[1] !='' && parseInt(startTimes[1]) === parseInt('30') && parseInt(id[2]) != parseInt(startTimes[1]) && parseInt(id[1]) ===  parseInt(startTimes[0])) {

                      callclickFlag = true;
                      element.setAttribute('clickFlag', 'true');    //Commented by mag (nsp to fill enddate half hour )

                    }else if ( parseInt(endTimes[1]) === parseInt('30')  && parseInt(id[1]) === parseInt(endTimes[0])  ) {
                      callclickFlag = true;

                      }
                  if(callclickFlag){
                    element.setAttribute('clickFlag', 'true');
                    let param = id[1] + '_' + (id[2] == '0' ? id[2] + '0' : id[2]) + '_' + (id[3] == '0' ? '7' : id[3]);
                    // element.addEventListener('click', () => {
                    //   this.showAvailabilityPopup(param);
                    // });
                  }
                //additional code added  by : nsp -  end here


                  }
                }
              });
            }
            let exist = false;
            items.forEach(item => {
              if (parseInt(id[1]) >= parseInt(item.starttime.split(':')[0]) && parseInt(id[1]) < parseInt(item.endtime.split(':')[0]))
                exist = true;
            });
            if (exist == false && element.getAttribute('clickFlag') == null) {
              element.setAttribute('clickFlag', 'true');
              let param = id[1] + '_' + (id[2] == '0' ? id[2] + '0' : id[2]) + '_' + (id[3] == '0' ? '7' : id[3]);
              // element.addEventListener('click', () => {
              //   this.showAvailabilityPopup(param);
              // });
            }
          }
          });
        }
        else {
          let elements = document.getElementsByClassName('weekDiv');
          for (let i = 0; i < elements.length; i++) {
            let element = <HTMLDivElement>elements.item(i);
            let id = element.id.split('_');
            element.setAttribute('clickFlag', 'true');
            let param = id[1] + '_' + (id[2] == '0' ? id[2] + '0' : id[2]) + '_' + (id[3] == '0' ? '7' : id[3]);
            // element.addEventListener('click', () => {
            //   this.showAvailabilityPopup(param);
            // });
          }
        }
      }
    }


    this.loader.dismiss();
  }

  //******************************* end getSavedAvailabilities()*******************************


  /**
   * Image choose popup
   */
  choosePopup() {
    var choosePopup = this.alertController.create({
      title: "Take a picture from",
      buttons: [
        {
          text: "Gallery",
          cssClass: "orange-button",
          handler: data => {
            this.getImageFromGallery();
          }
        },
        {
          text: "Camera",
          cssClass: "green-button",
          handler: data => {
            this.getImageFromCamera();
          }
        }
      ]
    });
    choosePopup.present();
  }

  /**
   * Edit profile
   */
  editProfile() {
    if (AppState.IsMember)
      //this.navCtrl.push(ProfileEditPage);
      this.navCtrl.push('UpdateMemberProfilePage')
    else
      // this.navCtrl.push(ProviderProfileEditPage)
      this.navCtrl.push('UpdateprofilePage')
  }
  /**
    * Update profile pic
    */
  async uploadPhoto_old() {
    // if (this.base64Image != "assets/imgs/camera.png") {
    console.log(this.base64Image)
    if (this.base64Image) {
      console.log('if base64Image')
      console.log(this.base64Image)
      let uploadRequest: FormData = new FormData();
      // uploadRequest.append('filename', this.base64Image, 'file1');
      uploadRequest.append('files[]', this.base64Image, 'file1');
      uploadRequest.append('fileflag', '3');
      uploadRequest.append('userid', AppState.UserCred.userid);
      if (!AppState.IsWemaLife)
        uploadRequest.append('companyid', AppState.UserCred.currentCompany.companyid);
      uploadRequest.append('auth', 'false');
      uploadRequest.append('filestatus', '1');
      uploadRequest.append('type', 'profilepic');
      uploadRequest.append('createdby', AppState.UserCred.userid);

      this.toastCtrl.create({
        message: 'Uploading profile pic,please wait...',
        duration: 2000
      }).present();

      console.log('uploadRequest')
      console.log(uploadRequest)
      // this.apiProvider.Post(AppConst.FILE_UPLOAD, uploadRequest).subscribe((uploadResponse) => {

      this.apiProvider.Post(AppConst.FILE_UPLOAD, uploadRequest).subscribe((uploadResponse) => {

        console.log('uploadResponse')
        console.log(uploadResponse)
        if (uploadResponse != null && uploadResponse['status'])
          this.toastCtrl.create({
            message: 'Profile picture uploaded successfully',
            duration: 2000
          }).present();
        else
          this.toastCtrl.create({
            message: 'Profile picture not uploaded',
            duration: 2000
          }).present();
      },
        (error) => {
          this.toastCtrl.create({
            message: 'Profile picture not uploaded',
            duration: 2000
          }).present();
        }
      );
    }

  }

  async uploadPhoto_ol() {
    console.log(this.base64Image)
    var param = {
      'userid': AppState.UserCred.userid,
      'companyid': AppState.UserCred.currentCompany.companyid,
      'auth': 'false',
      'fileflag': '3',
      'filestatus': '1',
      'type': 'profilepic',
      'createdby': AppState.UserCred.userid,

    }


    //let data =  this.apiProvider.uploadImage(AppConst.FILE_UPLOAD, this.base64Image,param);

    /*  if (data.response.status) {
       this.getCurrentUserProfile()
       this.toastCtrl.create({
         message: 'Updated successfully',
         duration: 2000
       }).present();

     } */
    /*   else {
        this.getCurrentUserProfile()

        this.toastCtrl.create({
          message: 'Something went wrong, please try again',
          duration: 2000
        }, ).present();
      } */

  }

  uploadPhoto() {
    console.log(this.base64Image)
    var loader = this.loadingController.create({
      content: "Please wait.."
    });
    loader.present();
    var url = AppConst.GetWemaBaseAddress() + AppConst.FILE_UPLOAD;
    // var url = AppConst.WEMA_AZURE_LIVE + AppConst.FILE_UPLOAD;
    var httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.append('content-type', 'multipart/form-data;');
    httpHeaders = httpHeaders.append('Source-Api', 'wemalife');
    if (AppState.UserCred && AppState.UserCred.hasOwnProperty('key') && AppState.UserCred.key != null && AppState.UserCred.key != '')
      httpHeaders = httpHeaders.append('Api-Key', AppState.UserCred.key);
    if (AppState.UserCred != null && AppState.UserCred['userid'] != '') {
      httpHeaders = httpHeaders.append('uid', AppState.UserCred['userid']);
      httpHeaders = httpHeaders.append('fid', "0");
    }
    //httpHeaders=httpHeaders.append('Access-Control-Allow-Origin','*');
    //httpHeaders=httpHeaders.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    const fileTransfer: FileTransferObject = this.transfer.create();
    var random = Math.floor(Math.random() * 10000000);

    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'files',
      fileName: random + Date.now() + ".jpg",
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "multipart/form-data",
      headers: { headers: httpHeaders },

      params: {
        'userid': AppState.UserCred.userid,
        'companyid': AppState.UserCred.currentCompany.companyid,
        'auth': 'false',
        'fileflag': '3',
        'filestatus': '1',
        'type': 'profilepic',
        'createdby': AppState.UserCred.userid,

      }
    }


    fileTransfer.upload(this.base64Image, url, options)

      .then((data) => {
        loader.dismiss();
        console.log(JSON.stringify(data.responseCode));
        this.getCurrentUserProfileedited()
      }, (err) => {
        loader.dismiss();
        console.log(err);
        return err;
      });
  }




  async getCurrentUserProfileedited() {
    var filters = Array<{ fieldname: string, fieldvalue: string, operators: string }>();
    filters.push({
      fieldname: 'userid',
      fieldvalue: AppState.UserCred.userid,
      operators: "Equal"
    });
    if (!AppState.IsWemaLife)
      filters.push({
        fieldname: 'companyid',
        fieldvalue: AppState.UserCred.currentCompanyId,
        operators: "Equal"
      });

    let request: any;
    if (AppState.IsMember)
      request = {
        app: true,
        auth: true,
        filter: filters,
        MemberId: AppState.UserCred.userid,
        filterproperty: null,
        CompanyId: null
      };
    else
      request = {
        app: true,
        auth: true,
        filter: filters,
        filterproperty: {
          offset: 0,
          orderby: "preferences",
          recordlimit: 0
        },
      };

    let response = AppState.IsMember ? await this.apiProvider.Post(AppConst.GET_USER_PROFILE, request).toPromise() : await this.apiProvider.Post(AppConst.GET_PROVIDER_PROFILE, request).toPromise();
    if (response != null && response.hasOwnProperty('records') && response['records'].length > 0) {
      let userCred = response['records']['0'];
      console.log(AppState.UserCred);
      for (let key in userCred) {
        if (AppState.UserCred.hasOwnProperty(key)) {
          if (AppState.UserCred[key] != userCred[key]) {
            console.log(key);
            AppState.UserCred[key] = userCred[key];
          }
        }
      }
      console.log(AppState.UserCred);
      this.storage.set('UserCred', JSON.stringify(AppState.UserCred));
      this.events.publish('updateuser');
      this.navCtrl.setRoot(this.navCtrl.getActive().component);
      //this.navCtrl.setRoot(ProfilePage);
      // this.navCtrl.pop();
    }
  }

  /////////////////////////////////////

  /**
   * Get edit form fields
   */
  async getEditFields() {
    var filterProperty =
    {
      offset: 0,
      recordlimit: 10,
      orderby: "formname"
    };

    var filters = [
      {
        fieldname: "formid",
        fieldvalue: AppState.IsMember ? ["1"] : ["2"],
        operators: "In",
        dir: null
      }];

    var filterRequest =
    {
      app: true,
      auth: false,
      filter: filters,
      filterproperty: filterProperty,
      CompanyId: null,
      MemberId: this.currentUser.userid
    };

    var response = await this.apiProvider.Post(AppConst.FORM_BUILDER, filterRequest).toPromise();
    if (response != null && response.hasOwnProperty('records') && response['records'].length > 0) {
      var allFields = response['records'][0]['formfields'];
      this.formFields = allFields.filter(x => x.name != 'membertype' && !x.hasOwnProperty('registerforsomeone') && x.type != 'paragraph');
      this.formFields.forEach(x => {
        switch (x.type) {
          case 'text':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : null;
            break;
          case 'password':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : null;
            break;
          case 'confirmpassword':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : null;
            break;
          case 'date':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : this.dob;
            break;
          case 'select':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : null;
            break;
          case 'radio-group':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : null;
            break;
          case 'textarea':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : null;
            break;
          case 'file':
            this.values[x.name] = this.userInfo.hasOwnProperty(x.name) ? this.userInfo[x.name] : null;
            break;
          default:
            this.values[x.name] = null;
            break;
        }
      });
      console.log(this.values);
    }
    //this.GetDocs()
  }

  /**
     * Get service categories
     */
  async getProviderTypes() {
    var filterProperty = {
      offset: 0,
      recordlimit: 0,
      orderby: "categoryorder"
    };

    var filter = {
      fieldname: "companyid",
      fieldvalue: this.companyId,
      operators: "Equal"
    };

    var filterRequest = {
      filterproperty: filterProperty,
      auth: false,
      filter: [filter]
    };

    let response = await this.apiProvider.Post(AppConst.SERVICE_CATEGORIES, filterRequest).toPromise();
    if (response != null && response.hasOwnProperty('totalrecord') && response['totalrecord'] > 0) {
      this.providerTypes = response['records'];
      this.providerTypes.forEach(element => {
        element['selected'] = false;
      });
    }
  }

  /**
   * Get the services
   */
  async getServices() {
    var result = this.providerTypes.filter(x => x.selected).map(function (a) { return a.categoryid; });
    if (result != null && result.length > 0) {
      var filterProperty =
      {
        offset: 0,
        recordlimit: 0,
        orderby: "servicename"
      };

      var filters: Array<{ fieldname: string, fieldvalue: any, operators: string }> = [];

      var filter =
      {
        fieldname: "servicecategoryid",
        fieldvalue: result,
        operators: "In"
      };

      var companyfilter =
      {
        fieldname: "companyid",
        fieldvalue: this.companyId,
        operators: "Equal"
      };

      filters.push(filter);
      filters.push(companyfilter);

      var filterRequest =
      {
        filterproperty: filterProperty,
        filter: filters,
        auth: false
      };

      let response = await this.apiProvider.Post(AppConst.GET_SERVICES, filterRequest).toPromise();
      if (response != null && response.hasOwnProperty('records')) {
        this.services = response['records'];
        this.services.forEach(element => {
          element['selected'] = false;
        });
      }
    }
    else
      this.services = null;
  }

  /**
   * Get provider skills
   */
  async getSkills() {
    let request = {
      app: true,
      auth: false,
      filter: [
        { fieldname: "skillstatus", fieldvalue: '1', operators: "Equal" },
      ],
      filterProperty: {
        offset: 0,
        orderby: "skill",
        recordlimit: 0
      },
    };

    let response = await this.apiProvider.Post(AppConst.GET_SKILLS, request).toPromise();
    if (response != null && response.hasOwnProperty('records')) {
      this.skills = response['records'];


      this.skills.forEach(element => {
        element['selected'] = false;
      });
    }
    this.selectedProviderSkills();

    //this.providerskills
  }
  /**
     * reset provider skills : after update
     */
  async selectedProviderSkills() {
    for (let i in this.skills) {
      // console.log('skills=',this.skills[i].skill)
      {
        for (let j in this.providerskills) {
          if (this.skills[i].skillid == this.providerskills[j]) {
            this.skills[i]['selected'] = true
            //console.log('true',this.providerskills[j])
          }
        }
      }
    }
  }

  /**
   * reset provider Interests : after update
   */
  async selectedProviderInterests() {
    for (let i in this.interests) {
      {
        for (let j in this.providerinterests) {
          if (this.interests[i].interestid == this.providerinterests[j]) {
            this.interests[i]['selected'] = true
          }
        }
      }
    }
  }

  /**
   * Get provider skills
   */
  async getInterests() {
    let request = {
      app: true,
      auth: false,
      filter: [
        { fieldname: "intereststatus", fieldvalue: '1', operators: "Equal" },
      ],
      filterProperty: {
        offset: 0,
        orderby: "interest",
        recordlimit: 0
      },
    };

    let response = await this.apiProvider.Post(AppConst.GET_INTERESTS, request).toPromise();
    if (response != null && response.hasOwnProperty('records')) {
      this.interests = response['records'];
      this.interests.forEach(element => {
        element['selected'] = false;
      });
    }
    this.selectedProviderInterests();
  }

  /**
   * Reset fields
   */
  reset() {
    this.getEditFields();
  }

  /**
   * Update user details
   */
  async save() {

    console.log('after saving this.values[\'password\'] : ', this.values['password']);
    console.log('after saving this.values[\'confirmpassword\'] : ', this.values['confirmpassword']);

    if(this.values['password'] != '' && this.values['confirmpassword'] != ''){
      this.values['password'] = this.password;
      this.values['confirmpassword'] = this.confirmPassword;

    }else{
      delete this.values['password'];
      delete this.values['confirmpassword'];
    }

    console.log('after the condition : for the password', this.values);
    var selectedProviderTypes = this.providerTypes.filter(x => x.selected).map(function (a) { return a.categoryid; });
    this.values['providertypes'] = (selectedProviderTypes != null && selectedProviderTypes.length > 0) ? selectedProviderTypes : this.currentUser['formvalues']['providertypes'];
    var selectedServices = (this.services != null && this.services.length > 0) ? this.services.filter(x => x.selected).map(function (a) { return a.serviceid; }) : null;
    this.values['services'] = (selectedServices != null && selectedServices.length > 0) ? selectedServices : this.currentUser['formvalues']['services'];
    var selectedSkills = this.skills.filter(x => x.selected).map(function (a) { return a.skillid; });
    //this.values['skills'] = (selectedSkills!=null&&selectedSkills.length>0)? selectedSkills:this.currentUser['formvalues']['skills'];

    this.values['skills'] = (this.providerskills != null && this.providerskills.length > 0) ? this.providerskills : this.currentUser['formvalues']['skills'];
    this.values['dateofbirth'] = this.dob;




    var selectedInterests = this.interests.filter(x => x.selected).map(function (a) { return a.interestid; });
    // this.values['interests'] = (selectedInterests!=null&&selectedInterests.length>0)? selectedInterests:this.currentUser['formvalues']['interests'];

    this.values['interests'] = (this.providerinterests != null && this.providerinterests.length > 0) ? this.providerinterests : this.currentUser['formvalues']['interests'];

    let request = {
      companyid: this.currentUser.currentCompany.companyid,
      action: 'U',
      usertypeid: '3',
      userid: this.currentUser.userid,
      updatedby: this.currentUser.userid,
      formid: '2',
      member: this.values
    };

    console.log('request after save() : ', request);
    let response = await this.apiProvider.Post(AppConst.PROVIDER_REGISTER, request).toPromise();
    if (response != null && response['status']) {



      this.toastCtrl.create({
        message: 'Updated successfully',
        duration: 2000
      }).present();
      this.getCurrentUserProfile();
      //this.navCtrl.pop();
    }
    else {
      this.toastCtrl.create({
        message: 'Something went wrong, please try again',
        duration: 2000
      }).present();
    }
  }

  /**
     * Password show toggle
     */
  showPassword(flag: boolean) {
    if (flag)
      this.showPass = !this.showPass;
    else
      this.showConfirmPass = !this.showConfirmPass;
  }

  async getCurrentUserProfile() {
    var filters = Array<{ fieldname: string, fieldvalue: string, operators: string }>();
    filters.push({
      fieldname: 'userid',
      fieldvalue: AppState.UserCred.userid,
      operators: "Equal"
    });
    if (!AppState.IsWemaLife)
      filters.push({
        fieldname: 'companyid',
        fieldvalue: AppState.UserCred.currentCompanyId,
        operators: "Equal"
      });

    let request: any;
    if (AppState.IsMember)
      request = {
        app: true,
        auth: true,
        filter: filters,
        MemberId: AppState.UserCred.userid,
        filterproperty: null,
        CompanyId: null
      };
    else
      request = {
        app: true,
        auth: true,
        filter: filters,
        filterproperty: {
          offset: 0,
          orderby: "preferences",
          recordlimit: 0
        },
      };

    let response = AppState.IsMember ? await this.apiProvider.Post(AppConst.GET_USER_PROFILE, request).toPromise() : await this.apiProvider.Post(AppConst.GET_PROVIDER_PROFILE, request).toPromise();
    if (response != null && response.hasOwnProperty('records') && response['records'].length > 0) {
      let userCred = response['records']['0'];
      console.log('AppState.UserCred : ', AppState.UserCred);
      for (let key in userCred) {
        if (AppState.UserCred.hasOwnProperty(key)) {
          if (AppState.UserCred[key] != userCred[key]) {
            console.log(key);
            AppState.UserCred[key] = userCred[key];
          }
        }
      }
      console.log(AppState.UserCred);
      this.storage.set('UserCred', JSON.stringify(AppState.UserCred));
      this.events.publish('updateuser');
      this.navCtrl.pop();

      this.providerskills = AppState.UserCred.providerskillsid;
      this.selectedProviderSkills();

      this.providerinterests = AppState.UserCred.providerinterestsid;
      this.selectedProviderInterests();
    }
  }

  next() {
    this.slides.slideNext();
  }

  prev() {
    this.slides.slidePrev();
  }
  public fileTransfer: FileTransferObject = this.transfer.create();

  UploadDocs() {
    console.log('in uplpoad docs')
    this.fileChooser.open()
      .then(uri => {
        console.log(uri)
        this.filenameExtension = uri.split('/')
        console.log(this.filenameExtension)
        this.Submit = false
        console.log(this.filenameExtension.length - 2)
        this.setextension = this.filenameExtension[this.filenameExtension.length - 1]
        console.log(this.setextension)
        this.DotExE = this.setextension.split('.')
        console.log("-----------------exte nsion ----------------", this.DotExE)
        this.filexe = this.DotExE[this.DotExE.length - 1]
        console.log("---------------------------------")
        if (this.filexe == 'PDF' || this.filexe == 'pdf' || this.filexe == 'PNG' || this.filexe == 'png' || this.filexe == 'JPG' || this.filexe == 'jpg') {
          if (this.filexe == 'PDF' || this.filexe == 'pdf') {
            this.formatename = '.pdf'
          }
          else if (this.filexe == 'PNG' || this.filexe == 'png') {
            this.formatename = '.png'
          }
          else if (this.filexe == 'JPG' || this.filexe == 'jpg') {
            this.formatename = '.jpg'
          }
          else if (this.filexe == 'GIF' || this.filexe == 'gif') {
            this.formatename = '.gif'
          }
        }
        else {
          this.toastCtrl.create({
            message: 'Please select jpg,png,gif and pdf only ',
            duration: 2000
          }).present();
        }

        this.newimage = uri
        console.log(uri)
        this.base64Image = uri;
        let filePath: string = uri;
        this.base64.encodeFile(filePath).then((base64File: string) => {
          console.log(base64File);
          //this.base64Image=base64File
        }, (err) => {
          console.log(err);
        });
      });
  }
  uploadrequest() {
    //this.Submit=true
    console.log(this.base64Image)
    // this.fileTransfer.onProgress((e)=>
    // {
    //   let prg=(e.lengthComputable) ?  Math.round(e.loaded / e.total * 100) : -1;
    //   console.log("progress:"+prg);
    // });

    var loader = this.loadingController.create({
      content: "Please wait.."
    });
    loader.present();
    var url = AppConst.GetWemaBaseAddress() + AppConst.FILE_UPLOAD;
    // var url = AppConst.WEMA_AZURE_LIVE + AppConst.FILE_UPLOAD;

    var httpHeaders = new HttpHeaders();

    httpHeaders = httpHeaders.append('content-type', 'multipart/form-data;');
    httpHeaders = httpHeaders.append('Source-Api', 'wemalife');
    if (AppState.UserCred && AppState.UserCred.hasOwnProperty('key') && AppState.UserCred.key != null && AppState.UserCred.key != '')
      httpHeaders = httpHeaders.append('Api-Key', AppState.UserCred.key);
    if (AppState.UserCred != null && AppState.UserCred['userid'] != '') {
      httpHeaders = httpHeaders.append('uid', AppState.UserCred['userid']);
      httpHeaders = httpHeaders.append('fid', "0");
    }
    //httpHeaders=httpHeaders.append('Access-Control-Allow-Origin','*');
    //httpHeaders=httpHeaders.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    var random = Math.floor(Math.random() * 100000);
    console.log(random)
    //option transfer
    let options: FileUploadOptions = {
      fileKey: 'files',
      fileName: random + Date.now() + this.formatename,
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "multipart/form-data",
      headers: { headers: httpHeaders },
      params: {
        'userid': AppState.UserCred.userid,
        'companyid': AppState.UserCred.currentCompany.companyid,
        'auth': 'false',
        'fileflag': '2',
        'filestatus': '1',
        'type': 'certificates',
        'createdby': AppState.UserCred.userid,
      }
    }
    console.log(this.base64Image, url, options)
    this.fileTransfer.upload(this.base64Image, url, options)
      .then((data) => {
        //**Progress Bar**
        loader.dismiss();
        console.log(JSON.stringify(data.responseCode));
        this.ResCode = data.responseCode

        if (this.ResCode == 200) {
          this.toastCtrl.create({
            message: 'Upload  successfully',
            duration: 2000
          }).present();

          //  this.uplodedata.push({Name: this.setextension, Code:this.ResCode})
          console.log(this.uplodedata)
        }

        else {
          this.toastCtrl.create({
            message: 'Something went wrong, please try again',
            duration: 2000
          }).present();
          //  this.uplodedata.push({Name: this.setextension, Code:'500'})
          console.log(this.uplodedata)
        }
        this.GetDocs()
      }, (err) => {
        loader.dismiss();
        console.log(err);
        this.toastCtrl.create({
          message: 'Something went wrong, please try again',
          duration: 2000
        }).present();
        this.ResCode = 400
        //  this.uplodedata.push({Name: this.setextension, Code:'500'})
        console.log(this.uplodedata)
        return err;
      });

  }


  async GetDocs() {

    let filters = [];
    filters.push({ fieldname: "userid", fieldvalue: AppState.UserCred.userid, operators: "Equal" });
    filters.push({ fieldname: "type", fieldvalue: "certificates", operators: "Equal" });
    let request = {
      filter: filters,
      filterproperty: { dir: "DESC", offset: 0, orderby: "fileid", recordlimit: 50 },
    };
    var response = await this.apiProvider.Post(AppConst.GET_FILE_PORVIDER, request).toPromise();
    console.log(response)

    if (response != null) {
      this.uplodedata = response['records']

      for (let i in this.uplodedata) {
        this.uplodedata[i].filename


        this.fileEXE = this.uplodedata[i].filename.split('.')

        this.uplodedata[i]['fileextension'] = this.fileEXE[this.fileEXE.length - 1]
      }
      console.log(this.uplodedata)
    }
  }


  // async Delete(i)
  // {
  // console.log('in delete function ------------',i)
  //   let request = {
  //     deletedby: "1",
  //     fileid: ""

  //   };
  //   var response = await this.apiProvider.Post(AppConst.DELETE_FILE_PORVIDER,request ).toPromise();
  // }


  async Delete(item: any) {

    let alert = this.alertCtrl.create({
      title: '',
      message: 'Do you want to remove ?',
      buttons: [
        {
          text: 'No',
          role: 'No',
          handler: () => {
            console.log('No clicked');
          }
        },
        {
          text: 'Yes',
          cssClass: "#08a79d",
          handler: () => {
            if (item.createdby == AppState.UserCred.userid) {
              var request =
              {
                deletedby: AppState.UserCred.userid,
                fileid: item.fileid
              };
              var result = this.apiProvider.Post(AppConst.DELETE_FILE_PORVIDER, request);
              result.subscribe((response) => {
                if (response != null && response['status']) {
                  if (response['status'] == true) {
                    this.toastCtrl.create({
                      message: 'Document deleted. ',
                      duration: 2000
                    }).present();

                    this.GetDocs();
                  }


                }
              },
                (err) => {

                });

            }
            else {

              this.toastCtrl.create({
                message: ' '

              })
            }

          }
        }
      ]
    });

    alert.present();
  }


  async getEmergencyContact() {
    console.log('inside getEmergencyContact : ', this.fieldName);

    if (this.apstate.IsMember) {
      this.fieldName = 'memberid';
    } else {
      this.fieldName = 'providerid';
    }
    console.log('inside getEmergencyContact : ', this.fieldName);

    var filters = [
      {fieldname: this.fieldName, fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];

    const req =
    {
    //  app: true,
      //auth: true,
      filter: filters,
      //date: "2018-10-30",
      filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "DESC" },
      companyid: AppState.UserCred.currentCompany.companyid,
      login_Userid: AppState.UserCred.userid,
      sourceapi: "wemalife"

    };

    console.log('request for the emergency contacts : ', req);
    // let req = {
    //   providerid: AppState.UserCred.userid
    // }
    let response = await this.apiProvider.Post(AppConst.EMERGENCYCONTACTLIST, req).toPromise();
    console.log('emergency contact lisy', response)
    if (response['totalrecord'] != 0) {

      this.EmergencyData = response['records']
    }
  }
  ionViewDidEnter() {
    this.getEmergencyContact()
    this.Recrument()
    this.RecrumentDocs()
    this.TraningList()
    this.PropertyList()
  }

  addContact() {

    this.navCtrl.push('AddEmergencyContactPage', { "action": "C" });

  }

  edit(item) {
    console.log(item)
    this.navCtrl.push('AddEmergencyContactPage', { "action": "U", "emergencydata": item });
  }
  async delete(item) {
    let request = {
      contactid: item.contactid
    }
    let response = await this.apiProvider.Post(AppConst.EMERGENCYCONTACTREMOVE, request).toPromise()
    if (response['status'] == 1) {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
      this.getEmergencyContact()

    }
    else {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
    }
  }

  addRecrutment(item) {
    this.navCtrl.push('AddRecruitmentPage', { "action": "C", "recruitmentdata": item });
  }
  editrecruiment(item) {
    this.navCtrl.push('AddRecruitmentPage', { "action": "U", "recruitmentdata": item });

  }


  addRecrutmentdocs() {

    this.navCtrl.push('AddRecruitmentDocsPage', { "action": "C", "recruitmentdata": '' });
  }


  async deleterec(item) {
    let req = {
      "recruitmentid": item.recruitmentid
    }
    let response = await this.apiProvider.Post(AppConst.RECURUITMENTDELETE, req).toPromise();
    if (response['status'] == 1) {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
      this.Recrument()
    }
    else {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
    }
  }

  async deleterecDocs(item) {

    let req = {
      "documentid": item.documentid
    }
    let response = await this.apiProvider.Post(AppConst.RECRUITMENTDOCREMOVE, req).toPromise();
    if (response['status'] == 1) {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
      this.RecrumentDocs()
    }
    else {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
    }
  }
  async Recrument() {
    // let req = {
    //   providerid: AppState.UserCred.userid
    // }

    var filters = [
      {fieldname: "providerid", fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];

    const req =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

    };

    let response = await this.apiProvider.Post(AppConst.RECRUETMENTLIST, req).toPromise();
    console.log('emergency contact lisy', response)
    if (response['totalrecord'] != 0) {
      this.Recrumentdata = response['records']
    }
  }

  async RecrumentDocs() {
    // let req = {
    //   providerid: AppState.UserCred.userid
    // }

    var filters = [
      {fieldname: "providerid", fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];

    const req =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

      };

    let response = await this.apiProvider.Post(AppConst.RECRUMENTDATA, req).toPromise();
    console.log('emergency contact lisy', response)
    if (response['totalrecord'] != 0) {
      this.RecrumentDocument = response['records']
    }

  }

  async TraningList() {
    // let req = {
    //   providerid: AppState.UserCred.userid
    // }

    var filters = [
      {fieldname: "providerid", fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];

    const req =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

      };

    let response = await this.apiProvider.Post(AppConst.TRANINGDOCLIST, req).toPromise();
    console.log('traning data  lisy', response)
    if (response['totalrecord'] != 0) {
      this.TraningList = response['records']
    }
  }
  async DeleteTraningdata(item) {
    let req = {
      "trainingid": item.trainingid
    }
    let response = await this.apiProvider.Post(AppConst.TRANINGDOCREMOVE, req).toPromise();
    if (response['status'] == 1) {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
      this.TraningList()
    }
    else {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
    }

  }



  async PropertyList() {
    // let req = {
    //   providerid: AppState.UserCred.userid
    // }
    var filters = [
      {fieldname: "providerid", fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];

    const req =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

      };


    let response = await this.apiProvider.Post(AppConst.PROPERTYLIST, req).toPromise();
    console.log('traning data  lisy', response)
    if (response['totalrecord'] != 0) {
      this.PropertyListdata = response['records']
    }
  }

  async ProviderCandidate() {
    console.log("you are in provide ProviderCandidateList")
    // let req = {
    //   providerid: AppState.UserCred.userid
    // }
    console.log('apstate is member : ', this.apstate.IsMember)
    console.log('this.fieldname : ', this.fieldName);
    var filters = [
      {fieldname: this.fieldName, fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];

    const req =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

    };

    let response = await this.apiProvider.Post(AppConst.PROVIDERCANDIDATELIST, req).toPromise();
    console.log('PROVIDERCANDIDATELIST lisy', response)
    if (response['totalrecord'] != 0) {
      this.PROVIDERCANDIDATELIST = response['records']
    }
  }
  async ProviderClient() {

    // let req = {
    //   providerid: AppState.UserCred.userid
    // }

    var filters = [
      {fieldname: this.fieldName, fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];

    const req =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

    };

    let response = await this.apiProvider.Post(AppConst.PROVIDERCLIENTLIST, req).toPromise();
    console.log('PROVIDERCANDIDATELIST lisy', response)
    if (response['totalrecord'] != 0) {
      this.ProvioderClientdata = response['records']
    }
  }

 async ReviewList(){

  // let req={
  //   providerid:AppState.UserCred.userid
  // }

  var filters = [
    {fieldname: this.fieldName, fieldvalue: AppState.UserCred.userid, operators: "Equal"}
   // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
  ];

  const req =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "ASC" },
        companyid: AppState.UserCred.currentCompany.companyid,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"

      };
  let res= await this .apiProvider .Post(AppConst.ReviewList,req).toPromise();
  if(res['totalrecord']!=0){

    this.Reviewdata=res['records']
  }
 }
  async DeletePropertydata(item){

    let req = {
      "propertyid": item.propertyid
    }
    let response = await this.apiProvider.Post(AppConst.PROPERTYREMOVE, req).toPromise();
    if (response['status'] == 1) {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
      this.PropertyList()
    }
    else {
      this.toastCtrl.create({
        message: response['message'],
        duration: 2000
      }).present();
    }


  }

  /**
   * Navigate to previous week
   */
  goToPrevWeek() {

    //clear the previous data
    if (document.querySelectorAll('.provider-cycle-saved-availability').length > 0) {
      Array.from(document.querySelectorAll('.provider-cycle-saved-availability')).forEach((a) =>{
        a.remove();
      });
    }

    this.currentNavigatedDateWeek = new Date(this.currentNavigatedDateWeek.getTime() - 7 * 24 * 60 * 60 * 1000);
  console.log(this.currentNavigatedDateWeek);
  this.currentDate=this.currentNavigatedDateWeek;
  console.log('currentDate : ', this.currentDate);
  this.getWeekDays();
  this.getSavedAvailabilities();
    // this.getWeekAppointments();
  }

   /**
   * Navigate to next week
   */
  goToNextWeek() {
    //clear the previous data
    if (document.querySelectorAll('.provider-cycle-saved-availability').length > 0) {
      Array.from(document.querySelectorAll('.provider-cycle-saved-availability')).forEach((a) =>{
        a.remove();
      });
    }

    this.currentNavigatedDateWeek = new Date(this.currentNavigatedDateWeek.getTime() + 7 * 24 * 60 * 60 * 1000);
  console.log(this.currentNavigatedDateWeek);
  this.currentDate=this.currentNavigatedDateWeek;
  console.log('currentDate : ', this.currentDate);
  this.getWeekDays();
  this.getSavedAvailabilities();
    // this.getWeekAppointments();
  }

  async getMemberAppointments(){

    
  }
}
