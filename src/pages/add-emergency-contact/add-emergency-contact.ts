import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Helper } from '../../helpers/helper';
import { AppState } from '../../AppStates';
import { AppConst } from '../../AppConst';
import { ApiProvider } from '../../providers/api/api';
/**
 * Generated class for the AddEmergencyContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-emergency-contact',
  templateUrl: 'add-emergency-contact.html',
})
export class AddEmergencyContactPage {
  createBills: any;
  Gender = ["Male", "Female", "Other"];
  Title = ["Prof", "Dr", "Mr", "Mdm", "Mrs", "Miss"]
  productForm: FormGroup;
  data = {
    "action": "",
    "companyid": "",
    "providerid": "",
    "contactid":"",
    "createdby": "",
    "firstname": "",
    "title": "",
    "enddate": "",
    "surname": "",
    "relation": "",
    "phone": "",
    "startdate": "",
  }
  // data: { Gender: '', Firstname: '', Surname: '',relation:'',phonenumber:'' ,startdate: '' ,enddate:''};
  isEmpty: boolean = false;
  editdata: any;

  constructor(private toastCtrl: ToastController, public navCtrl: NavController, public fb: FormBuilder, private apiProvider: ApiProvider, private viewCtrl: ViewController, public navParams: NavParams) {
    console.log(navParams.get('emergencydata'))
    if (navParams.get('emergencydata') != undefined) {
      this.editdata = navParams.get('emergencydata')
      this.data.firstname = this.editdata.firstname
      this.data.surname = this.editdata.surname
      this.data.phone = this.editdata.phone
      this.data.startdate = this.editdata.startdate
      this.data.title = this.editdata.title
      this.data.relation = this.editdata.relation
      this.data.enddate = this.editdata.enddate
      this.data.contactid=this.editdata.contactid
    }
    // this.data=navParams.get('emergencydata')


    this.productForm = fb.group({
      'Gender': [null, Validators.compose([Validators.required])],
      'Firstname': [null, Validators.compose([Validators.required])],
      'Surname': [null, Validators.compose([Validators.required])],
      'relation': [null, Validators.compose([Validators.required])],
      'phonenumber': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      'startdate': [null, Validators.compose([Validators.required])],
      'enddate': [null, Validators.compose([Validators.required])],
    });
  }

  ionViewDidLoad() {

    console.log(this.Title)
    let date = new Date(Date.now());
    console.log('ionViewDidLoad AddEmergencyContactPage');


  }
  closeModal() {
    this.navCtrl.pop()
    // this.viewCtrl.dismiss('');
  }
  async  submit(data) {
console.log(new Date (this.data.startdate),new Date (this.data.enddate))
   if( new Date(this.data.startdate) >=   new Date(this.data.enddate)) {
    this.toastCtrl.create({
      message: 'Please enter the valid date ',
      duration: 2000
    }).present();
    }
    else{
      console.log(' condition false ')
      this.data.providerid = AppState.UserCred.userid
      this.data.companyid = AppState.UserCred.currentCompany.companyid
      this.data.createdby = AppState.UserCred.userid
      this.data.action = this.navParams.get('action')
      console.log('now we will call the api data ', data)
      let request = data
      let response = await this.apiProvider.Post(AppConst.Emergencycud, request).toPromise()
      if (response['status']) {
        this.toastCtrl.create({
          message: 'Emergency Contact uploaded successfully',
          duration: 2000
        }).present();
        this.navCtrl.pop();
      }
      else {
        this.toastCtrl.create({
          message: 'Emergency Contact  not uploaded ',
          duration: 2000
        }).present();
      }
    }


  }
}
