import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddEmergencyContactPage } from './add-emergency-contact';

@NgModule({
  declarations: [
    AddEmergencyContactPage,
  ],
  imports: [
    IonicPageModule.forChild(AddEmergencyContactPage),
  ],
})
export class AddEmergencyContactPageModule {}
