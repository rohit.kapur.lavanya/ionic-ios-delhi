import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Helper } from '../../helpers/helper';
import { AppState } from '../../AppStates';
import { AppConst } from '../../AppConst';
import { ApiProvider } from '../../providers/api/api';
/**
 * Generated class for the AddRecruitmentDocsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-recruitment-docs',
  templateUrl: 'add-recruitment-docs.html',
})
export class AddRecruitmentDocsPage {
  createBills: any;
  Gender = ["Male", "Female", "Other"];
  Title = ["Passport", "Driving License", "Birth Certificate", "Home Office Letter", "Proof of Address", "Reference 1", "Reference 2", "DBS Certificate"];
  Source = ['Job Board ', 'Website', 'Flyer', 'Word of Mouth', 'Other'];
  decision = ['Accept', 'Decline'];
  productForm: FormGroup;
  firstname: any
  lastname: any
  type: any = "ADD"
  title: any
  data = {
    "action": "",
    "providerid": "",
    "createdby": "",
    "documentrefer": "",
    "doclink": "",
    "decisiondate": "",
    "acceptdate": "",
    "documenttype": "",
    "validdate": "",
    "decision": "",
    "documentid": "0",
    "recruitmentid":""
  }
  // data: { Gender: '', Firstname: '', Surname: '',relation:'',phonenumber:'' ,startdate: '' ,enddate:''};
  isEmpty: boolean = false;
  editdata: any;

  constructor(private toastCtrl: ToastController, public navCtrl: NavController, public fb: FormBuilder, private apiProvider: ApiProvider, private viewCtrl: ViewController, public navParams: NavParams) {
    console.log(navParams.get('recruitmentdata'))

    if (navParams.get('recruitmentdata') != undefined) {


    }
    // this.data=navParams.get('emergencydata')


    this.productForm = fb.group({
      'Gender': [null, Validators.compose([Validators.required])],
      'Firstname': [null, Validators.compose([Validators.required])],
      'Surname': [null, Validators.compose([Validators.required])],
      // 'relation': [null, Validators.compose([Validators.required])],
      // 'phonenumber': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      //  'startdate': [null, Validators.compose([Validators.required])],
      //'enddate': [null, Validators.compose([Validators.required])],
      'applicationsubmission': [null, Validators.compose([Validators.required])],
'interview1': [null, Validators.compose([Validators.required])],
      'decision': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      'source': [null, Validators.compose([Validators.required])],
      'decisiondate': [null, Validators.compose([Validators.required])],
    });
  }

  ionViewDidLoad() {

    console.log(this.Title)
    let date = new Date(Date.now());
    console.log('ionViewDidLoad document data');


  }
  closeModal() {
    this.navCtrl.pop()
    // this.viewCtrl.dismiss('');
  }
  async submit(data) {

    console.log(' condition false ')
    this.data.providerid = AppState.UserCred.userid
    this.data.createdby = AppState.UserCred.userid
    this.data.action = "C"
    console.log(this.data.action)
    console.log('now we will call the api data ', data)
    let request = data
    let response = await this.apiProvider.Post(AppConst.RECRUMENTDOCUMENTCUD, request).toPromise()
    if (response['status']) {
      this.toastCtrl.create({
        message: ' Recruitment  uploaded successfully',
        duration: 2000
      }).present();
      this.navCtrl.pop()
      // this.GETRECRUMENTDATA()
    }
    else {
      this.toastCtrl.create({
        message: ' Recruitment   not uploaded ',
        duration: 2000
      }).present();
    }
  }

  async GETRECRUMENTDATA() {
    let request = {
      "providerid": AppState.UserCred.userid


    }
    let response = await this.apiProvider.Post(AppConst.RECRUMENTDATA, request).toPromise()


    console.log(response)
  }
}
