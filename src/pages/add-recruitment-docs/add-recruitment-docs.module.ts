import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRecruitmentDocsPage } from './add-recruitment-docs';

@NgModule({
  declarations: [
    AddRecruitmentDocsPage,
  ],
  imports: [
    IonicPageModule.forChild(AddRecruitmentDocsPage),
  ],
})
export class AddRecruitmentDocsPageModule {}
