import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Helper } from '../../helpers/helper';
import { AppState } from '../../AppStates';
import { AppConst } from '../../AppConst';
import { ApiProvider } from '../../providers/api/api';
/**
 * Generated class for the AddRecruitmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-recruitment',
  templateUrl: 'add-recruitment.html',
})
export class AddRecruitmentPage {
  createBills: any;
  Gender = ["Male", "Female", "Other"];
  Title = ["Prof", "Dr", "Mr", "Mdm", "Mrs", "Miss"];
  Source=['Job Board ','Website','Flyer','Word of Mouth','Other'];
  decision=['Accept','Decline'];
  productForm: FormGroup;
  firstname :any
  lastname:any
  type:any ="ADD"
  title:any
    data = {
    "action": "",
    "companyid": "",
    "providerid": "",
    "contactid":"",
    "createdby": "",
    "applicationsubmission":"",
    "decisiondate":"",
    "source":"",
    "othersource":"",
    "interview1": "",
    "interview2": "",
    "interview3": "",
    "decision": "",
    "recruitmentid":""
  }
  // data: { Gender: '', Firstname: '', Surname: '',relation:'',phonenumber:'' ,startdate: '' ,enddate:''};
  isEmpty: boolean = false;
  editdata: any;

  constructor(private toastCtrl: ToastController, public navCtrl: NavController, public fb: FormBuilder, private apiProvider: ApiProvider, private viewCtrl: ViewController, public navParams: NavParams) {
    console.log(navParams.get('recruitmentdata'))

    if (navParams.get('recruitmentdata') != undefined) {


      this.editdata = navParams.get('recruitmentdata')
      this.type="EDIT"
    this.firstname = this.editdata.fname
    this.lastname = this.editdata.lname
    this.data.applicationsubmission=this.editdata.applicationsubmission
    this.data.source=this.editdata.source
    // var date1 = new Date(this.editdata.interview1).toISOString()
    // var date2 = new Date(this.editdata.interview1).toISOString()
    // var date = new Date(this.editdata.interview1).toISOString()
    this.data.interview1=this.editdata.interview1
    this.data.interview2=this.editdata.interview2
    this.data.interview3=this.editdata.interview3
    this.data.decision=this.editdata.decision
    this.data.decisiondate=this.editdata.decisiondate
    // this.data.surname =
      // this.data.phone = this.editdata.phone
      // this.data.startdate = this.editdata.startdate
      // this.data.title = this.editdata.title
      // this.data.relation = this.editdata.relation
      // this.data.enddate = this.editdata.enddate
      // this.data.contactid=this.editdata.contactid
    }
    // this.data=navParams.get('emergencydata')


    this.productForm = fb.group({
      'Gender': [null, Validators.compose([Validators.required])],
      'Firstname': [null, Validators.compose([Validators.required])],
      'Surname': [null, Validators.compose([Validators.required])],
        // 'relation': [null, Validators.compose([Validators.required])],
       // 'phonenumber': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      //  'startdate': [null, Validators.compose([Validators.required])],
      //'enddate': [null, Validators.compose([Validators.required])],
      'applicationsubmission': [null, Validators.compose([Validators.required])],
      'interview1': [null, Validators.compose([Validators.required])],
      'interview2': [null, Validators.compose([Validators.required])],
      'interview3': [null, Validators.compose([Validators.required])],
      'decision': [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
      'source': [null, Validators.compose([Validators.required])],
      'decisiondate': [null, Validators.compose([Validators.required])],
    });
  }

  ionViewDidLoad() {

    console.log(this.Title)
    let date = new Date(Date.now());
    console.log('ionViewDidLoad AddEmergencyContactPage');


  }
  closeModal() {
    this.navCtrl.pop()
    // this.viewCtrl.dismiss('');
  }
  async  submit(data) {
  // console.log(new Date (this.data.startdate),new Date (this.data.enddate))
  // if( new Date(this.data.startdate) >=   new Date(this.data.enddate)) {
    // this.toastCtrl.create({
    //   message: 'Please enter the valid date ',
    //   duration: 2000
    // }).present();
    // }
    // else{
      console.log(' condition false ')
      this.data.providerid = AppState.UserCred.userid
      this.data.companyid = AppState.UserCred.currentCompany.companyid
      this.data.createdby = AppState.UserCred.userid


      this.data.action = this.navParams.get('action')
console.log(this.data.action)
      if(this.data.action=="U"){
        this.data.recruitmentid=this.editdata.recruitmentid
      }
      console.log('now we will call the api data ', data)
      let request = data
      let response = await this.apiProvider.Post(AppConst.RECRUITMENTCUD, request).toPromise()
      if (response['status']) {
        this.toastCtrl.create({
          message: ' Recruitment  uploaded successfully',
          duration: 2000
        }).present();
        this.navCtrl.pop()
       // this.GETRECRUMENTDATA()
      }
      else {
        this.toastCtrl.create({
          message: ' Recruitment   not uploaded ',
          duration: 2000
        }).present();
      }
    }

 async GETRECRUMENTDATA(){
let request={
  "providerid": AppState.UserCred.userid
  // companyid:  AppState.UserCred.currentCompany.companyid,
  // filter: [{fieldname: "providerid", fieldvalue:  AppState.UserCred.userid, operators: "Equal"}],
  // filterproperty: {offset: 0, recordlimit: 500, orderby: "id", dir: "DESC"},
  // dir: "DESC",
  // offset: 0,
  // orderby: "id",
  // recordlimit: 500,
  // login_Userid:  AppState.UserCred.userid

}
  let response = await this.apiProvider.Post(AppConst.RECRUMENTDATA, request).toPromise()


  console.log(response)
}

}
