import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRecruitmentPage } from './add-recruitment';

@NgModule({
  declarations: [
    AddRecruitmentPage,
  ],
  imports: [
    IonicPageModule.forChild(AddRecruitmentPage),
  ],
})
export class AddRecruitmentPageModule {}
