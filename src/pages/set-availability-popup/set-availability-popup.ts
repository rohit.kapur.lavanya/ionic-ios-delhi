import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from '../../../node_modules/ionic-angular/navigation/view-controller';
import { ApiProvider } from '../../providers/api/api';
import { AppConst } from '../../AppConst';
import { AppState } from '../../AppStates';
import { DatePicker } from '../../../node_modules/@ionic-native/date-picker';

/**
 * Generated class for the SetAvailabilityPopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-set-availability-popup',
  templateUrl: 'set-availability-popup.html',
})
export class SetAvailabilityPopupPage {
  Data: Object;
  classdata: string;
  buttonClicked: boolean=true;
  Date: Date;
  Select_Location: boolean=false;
  startTimedata:any
  endTimedata:any
  dayofWeek:any
  clinicname: any;
  clinic_name: any;
  Slots=[1];
  constructor(public navCtrl: NavController, private datePicker: DatePicker ,private apiProvider :ApiProvider, private viewCtrl:ViewController,public navParams: NavParams) {
  console.log(navParams.data)
  this.startTimedata=navParams.data.starttime
  this.dayofWeek=navParams.data.dayofweek
  console.log(this.startTimedata)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetAvailabilityPopupPage');
    this.getClinicListpage()
  }
  cancel()
  {
    this.viewCtrl.dismiss( )


  }
  update()
{


  this.viewCtrl.dismiss(this.clinic_name,this.endTimedata)
} 
 Home(Home:any,$event)
  {

    console.log(Home)
    this.clinicname='0'
   this.Select_Location=false

    // this.classdata=' background: #13a89e !important;border-radius: 5px; padding: 8.5px;   color: #fff !important;  font-size: 14px; font-weight: 500; text-decoration: none; font-family:Muli, sans-serif; border: none !important;'  
  }
  Clinic()
  {
    this.Select_Location=true
    console.log(this.clinic_name)
    this.clinicname=this.clinic_name
  }
ChooseDate()
{
  this.buttonClicked=false
}

chooseWeek()
{
  this.buttonClicked=true
}
  async getClinicListpage() {
    var filters = [
      {fieldname: "provider_id", fieldvalue: AppState.UserCred.userid, operators: "Equal"}
     // { fieldname: "duration", fieldvalue: this.searchData.duration.slice(' ')['0'], operators: "Equal" }
    ];
    
    var request =
      {
      //  app: true,
        //auth: true,
        filter: filters,
        //date: "2018-10-30",
        filterproperty: { offset: 0, recordlimit: 500, orderby: "id", dir: "DESC" },
        companyid: AppState.UserCred.companyid,
        companyno: AppState.UserCred.companyno,
        login_Userid: AppState.UserCred.userid,
        sourceapi: "wemalife"
      //  MemberId: AppState.UserCred.userid,
      
      };
    var response = await this.apiProvider.Post(AppConst.clinic, request).toPromise();
   this.Data=response['records']
   
    }


      
  toggleAccordion1(){
    this.datePicker.show({
    date: new Date(),
    mode: 'date',
    androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
  }).then(
  
    date =>{
      console.log('Got date: ', date)

      this.Date=date
    
    } ,
  
    err => console.log('Error occurred while getting date: ', err)
  );
 
 
}

AddSlot()
{
  this.Slots.push(1)

}
}
