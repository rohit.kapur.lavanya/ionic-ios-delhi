import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { AppConst } from '../../AppConst';

/**
 * Generated class for the EmergencyContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-emergency-contact',
  templateUrl: 'emergency-contact.html',
})
    export class EmergencyContactPage {

  constructor(public navCtrl: NavController, public navParams: NavParams ,public apiProvider:ApiProvider) {
  }

      ionViewDidLoad() {
        console.log('ionViewDidLoad EmergencyContactPage');
      }


      async getEmergencyContact(){

      let req = {
        providerid:''
      }
      let res =await this.apiProvider.Post(AppConst.EMERGENCYCONTACTLIST,req).toPromise()

      }


      addContact(){


        this.navCtrl.push('ExpensespopupPage');

    }
}
